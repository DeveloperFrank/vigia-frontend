import React, { Component } from 'react';
import { Switch, Route, Link, BrowserRouter as Router } from 'react-router-dom'

import { Home } from './pages/Home'
// import { Suppliers } from './pages/Suppliers'
import { Sales } from './pages/Sales'
import { Inventory } from './pages/Inventory'
import { Reports_info } from './pages/Reports_info'
import { Primer } from './pages/Primer'
import { Operational } from './pages/Operational'
import { Admin } from './pages/Admin'
import { Parameter } from './pages/Parameter'
 
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <div className="Menu_main">
            <div className="menu">
              <p><Link to={'/'}>Home</Link></p>
              <p><Link to={'/Sales'}>Ventas</Link></p>
              <p><Link to={'/Inventory'}>Inventarios</Link></p>
              <p><Link to={'/Reports_info'}>Reportes</Link></p>
              <p><Link to={'/Primer'}>Cartilla</Link></p>
              <p><Link to={'/Operational'}>Gastos Operativos</Link></p>
              <p><Link to={'/Admin'}>Gastos Administrativos</Link></p>
              <p><Link to={'/Parameter'}>Parametrización</Link></p>
            </div>
            <div className="content">
              <Switch>
                <Route exact path='/' component={ Home } />
                <Route exact path='/Sales' component={ Sales } />
                <Route exact path='/Inventory' component={ Inventory } />
                <Route exact path='/Reports_info' component={ Reports_info } />
                <Route exact path='/Primer' component={ Primer } />
                <Route exact path='/Operational' component={ Operational } />
                <Route exact path='/Admin' component={ Admin } />
                <Route exact path='/Parameter' component={ Parameter } />
              </Switch>
            </div>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
