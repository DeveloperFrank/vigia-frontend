import React, { Component } from 'react'

export class Supplier extends Component {
	state = {
		dataSupplier: []
	}

	componentDidMount() {
		fetch('https://ec2-34-201-52-128.compute-1.amazonaws.com:8000/vigia/suppliers/?format=json')
			.then(response => response.json())
			.then(results => {
				this.setState({ dataSupplier: results })
				//console.log(this.state.dataSupplier)
		})	
	}

	render() {

		let dataSup = this.state.dataSupplier

		return(
			<div>
				<h4>Lista de proveedores</h4>
				<hr />
				<div className="cnt-create">
					<a className="register" href="#">Crear Registro</a>
				</div>
				<div className="table-info">
					<table>
						<thead>
							<tr>
								<th>Nombre</th>
								<th>@Correo</th>
								<th>Telefono</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							{
								dataSup.map((supplier, index)=>{
									return <tr key={ index }>
											<td>{ supplier.name }</td>
											<td>{ supplier.email }</td>
											<td>{ supplier.phone_number }</td>
											<td> <a className="link-actions" href="#">Editar</a>  <a className="link-actions" href="#">Eliminar</a></td>
										   </tr>
								})
							}
						</tbody>
					</table>
				</div>
			</div>
		)

	}

	
}