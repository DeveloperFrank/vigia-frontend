import React, { Component } from 'react';
//import Modal from 'react-modal';
import $ from 'jquery';
import 'jquery-ui-bundle';
import 'jquery-ui-bundle/jquery-ui.css';
import axios from 'axios';
import Select, { components } from 'react-select';

const suppliers = [];
const products = [];
const units = [];

const Placeholder = (props) =>{
	return(
		<components.Placeholder {...props} />
	);
}

let product_info = [];
var total_price = 0;
let array_product = [];
let id_purchases = 0;
 
export class Compras extends Component {

	constructor(props) {
		super(props);

		this.state = {
			date: '',
			supplier: '',
			transaction_type: '',
			status: '',
			proveedor_name: '',
			invoice_number: '',
			cost_senter: '',
			producto: [],
			// total: []
		}
	}

	addProvider = () => {
		if(document.getElementById('fecha').value !== "" && document.getElementById('proveedor').value !== "" && document.getElementById('numero').value !== ""){
			
			let supplier = [];

		
			supplier.transaction_type = "purchase";
			supplier.status = "executed";
			supplier.date =  document.getElementById('fecha').value; //$('#fecha').datepicker({dateFormat: 'yyyy-mm-dd'}).val();
			supplier.invoice_number = this.state.numero;
			supplier.cost_center = 1;
			supplier.supplier = this.state.proveedor;
			
			this.setState({
				date: document.getElementById('fecha').value,
				supplier: this.state.proveedor,
				invoice_number: this.state.numero,
				transaction_type: "purchase",
				status: 'executed'
				
			}, function(){
				axios.post('https://ec2-34-201-52-128.compute-1.amazonaws.com:8000/vigia/purchases/', {
					date: supplier.date,
					supplier: supplier.supplier,
					invoice_number: supplier.invoice_number,
					transaction_type: "purchase",
					status: 'executed',
					cost_center: supplier.cost_center
				})
				.then((response)=> {
					id_purchases = response.data.id;
				})
			});
			$('.form__info-product').addClass('show');
			
		}else{
			alert('Por favor ingrese información en todos los campos');
		}
	}

	calculateTotal = () => {
		total_price = 0;
		for(var i = 0; i < this.state.producto.length; i++) {
			total_price += this.state.producto[i].total;
		}
	}

	
	addProducto = () =>{
		let info_prod = []
		let date = new Date();
		info_prod.id = date.getTime();
		info_prod.cantidad = document.getElementById('cantidad').value;
		info_prod.total =  parseInt(document.getElementById('pu').value);
		info_prod.imp = document.getElementById('iva').value;
		console.log(info_prod);
		array_product.push(info_prod);

		this.setState({
			producto: array_product
		})
		
		$('#cantidad').val('');
		$('#pu').val('');
		$('#iva').val('');

		this.calculateTotal();
	}

	editProducto = (event, index) => {
		let edit = this.state.producto;
		if(edit[index].id === event){
			$('#cantidad').val(edit[index].cantidad);
			$('#pu').val(edit[index].total);
			$('#iva').val(edit[index].imp);
			edit.splice(index, 1);
			this.setState({
				producto: edit
			});
		}
		this.calculateTotal();
	}

	delProducto = (event, index) => {
		let del = this.state.producto;

		if(del[index].id === event ){
			del.splice(index, 1);
		}

		this.setState({
			producto: del
		});
		this.calculateTotal();
	}

	getValueNumber = (e) => {
		this.setState({
			numero: e.target.value
		})
	}

	handleChange_select = (e) => {
		this.setState({
			proveedor: e.value,
			proveedor_name: e.label
		})
		//console.log(e.value)
	}

	handleChange_select_products = (e) => {
		//console.log(e)
		product_info.product = e.value
		product_info.producto_nombre = e.label
	}

	handleChange_select_units = (e) => {
		//console.log(e)
		product_info.unidad = e.label
		product_info.uni_id = e.value
	}

	componentDidMount() {
		//get suppliers
		axios.get('https://ec2-34-201-52-128.compute-1.amazonaws.com:8000/vigia/suppliers/?format=json')
			.then(response => {
				//suppliers  = response.data;
				response.data.map(supplier => {
					suppliers.push({label: supplier.name, value: supplier.id});
					
				})
				//console.log(suppliers)
				this.setState({ suppliers: suppliers });
			});

		//get units
		axios.get('https://ec2-34-201-52-128.compute-1.amazonaws.com:8000/vigia/units/?format=json')
			.then(response => {
				response.data.map(unit =>{
					units.push({label: unit.name, value: unit.id});
				});

				this.setState({ units: units });

			});

		//get products
		axios.get('https://ec2-34-201-52-128.compute-1.amazonaws.com:8000/vigia/products/?format=json')
			.then(response => {
				response.data.map(product =>{
					products.push({ label: product.name, value: product.id });
				})

				this.setState({ products: products });
			});
	    $.datepicker.regional['es'] = {
	        closeText: 'Cerrar',
	        prevText: '< Ant',
	        nextText: 'Sig >',
	        currentText: 'Hoy',
	        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
	        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
	        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
	        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
	        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
	        weekHeader: 'Sm',
			dateFormat: 'yy-mm-dd',
			minDate: -60,
			maxDate: 0,
	        firstDay: 1,
	        isRTL: false,
	        showMonthAfterYear: false,
	        yearSuffix: ''
	    };

	    $.datepicker.setDefaults($.datepicker.regional['es']);
      	$(function () {
	    	$("#fecha").datepicker();
     	});
	}

	render () {
		return(
			<div>
				<h2>Compras</h2>
				<div className="form_add">
					<div className="ct-form">
						<input id="fecha" className="inp_vg" name="fecha" placeholder="Fecha de compra" type="text" />
						<Select id="proveedor" placeholder= {'Proveedor'} onChange={ opt => this.handleChange_select(opt) } options={this.state.suppliers} />
						<input id="numero" className="inp_vg" name="numero" placeholder="Numero Compra" maxLength="255" onChange={this.getValueNumber}/>
						<input id="monto_total" className="m_total inp_vg" placeholder="Costo total compra" type="number" />
						<button id="btn-add" className="btn-add inp_vg" onClick={this.addProvider}>Confirmar datos</button>
					</div>

					<hr />
					<div className="form_info">
						<div className="col-4 form__info-product">
							<div className="form-compra" >
								<input id="cantidad" placeholder="Cantidad" type="number" />
								<input id="pu" placeholder="Precio total" type="number" />
								<input id="iva" placeholder="Porcentaje IVA" type="number" />
								<Select id="producto" placeholder= {'Producto'} onChange={ opt => this.handleChange_select_products(opt) } options={this.state.products} />
								<Select id="unidad" placeholder= {'Unidad'} onChange={ opt => this.handleChange_select_units(opt) } options={this.state.units} />
								<button className="btn-add" onClick={this.addProducto}>Agregar Producto</button>
							</div>
						</div>
						<div className="col-1-4">
						  <table className="table">
						    <thead>
						      <tr>
						        <th>Nombre Producto</th>
						        <th>Unidad</th>
						        <th>Cantidad</th>
						        <th>Precio Unitario</th>
						        <th>Total</th>
						        <th>Acciones</th>
						      </tr>
						    </thead>
						    <tbody>
						     { this.state.producto.map((producto, index) => {
								return (
									<tr className="prod_info" key={index}>
										<td>{producto.producto_nombre}</td>
										<td>{producto.unidad}</td>
										<td>{producto.cantidad}</td>
										<td>$ {producto.total / producto.cantidad}</td>
										<td className="total">$ { producto.total }</td>
										<td>
											<button className="btn-del" onClick={() => this.editProducto(producto.id, index)}><i className="fas fa-edit fa-lg"></i></button>
											<button className="btn-del" onClick={() => this.delProducto(producto.id, index)}><i className="far fa-trash-alt fa-lg"></i></button>
										</td>
									</tr>
								)
							  }) 
							}
							<tr>
								<td className="total_price">Total: $ {total_price} </td>
							</tr>
						    </tbody>
						  </table>
						  <button id="btn-save">Guardar <i className="far fa-save fa-lg"></i></button>
						</div>
						<div className="col-4">
							<p>content-info</p>
						</div>
					</div>
				</div>
			</div>
		)
	}
}