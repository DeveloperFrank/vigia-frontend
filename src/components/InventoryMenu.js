import React, { Component } from 'react'
import { Compras } from './Compras'
import { OrdenProduccion } from './Produccion'
import { Transferencias } from './Transferencias'
import { Switch, Route, Link, BrowserRouter as Router } from 'react-router-dom'


export class InventoryMenu extends Component {
	render() {
		return(
			<Router>
				<div className="main_content">
					<div className="submenu">
						<Link className="link" to={'/Inventory/Compras'}>Compras</Link>
						<Link className="link" to={'/Inventory/Produccion'}>Orden de producción</Link>
						<Link className="link" to={'/Inventory/Transferencias'}>Transferencias</Link>
						<Link className="link" to={'/Inventory/Despiece'}>Despiece</Link>
						<Link className="link" to={'/Inventory/Ensamble'}>Ensamble</Link>
						<Link className="link" to={'/Inventory/Entradas'}>Entradas</Link>
					    <Link className="link" to={'/Inventory/Salidas'}>Salidas</Link>
						<Link className="link" to={'/Inventory/ConteoTotal'}>Conteo fisico total</Link>
						<Link className="link" to={'/Inventory/ConteoControl'}>Conteo fisico control</Link>
						<Link className="link" to={'/Inventory/Saldos'}>Saldos negativos</Link>
						<Link className="link" to={'/Inventory/Inicial'}>Inventario inicial</Link>
						<Link className="link" to={'/Inventory/Reportes'}>Reportes</Link>
					</div>
					<div className="main_sub">
						<Switch>
			            	<Route exact path='/Inventory/Compras' component={ Compras } />
			            	<Route exact path='/Inventory/Produccion' component={ OrdenProduccion } />
			            	<Route exact path='/Inventory/Transferencias' component={ Transferencias } />
			            </Switch>
					</div>
				</div>
			</Router>
			
		);
	}
}