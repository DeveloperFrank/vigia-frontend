import React, { Component } from 'react'
import { InventoryMenu } from '../components/InventoryMenu'

export class Inventory extends Component {
	render() {
		return(
			<div>
				<h1>Inventarios</h1>
				<InventoryMenu />
			</div>
		)
	}
}